package project.university.com.ichatapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;

public class LoginActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button loginBtn;
    FirebaseAuth auth;
    ProgressDialog progressDialog;
    DatabaseReference userDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        toolbar = findViewById(R.id.toolbar);
        auth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Login");
        TextInputLayout emailField = ((TextInputLayout) findViewById(R.id.user_email));
        TextInputLayout passwordField = ((TextInputLayout) findViewById(R.id.user_password));
        userDatabase = FirebaseDatabase.getInstance().getReference().child("users");
        loginBtn = findViewById(R.id.login_btn);
        loginBtn.setOnClickListener((view) -> {
            String userEmail = emailField.getEditText().getText().toString();
            String userPassword = passwordField.getEditText().getText().toString();
            System.out.println("email: " + userEmail + " ,,,, password : " + userPassword);
            if (isValidUserInfo(userEmail, userPassword)) {
                showProgressDialog();
                loginUser(userEmail, userPassword);
            }
        });
    }

    private void loginUser(String userEmail, String userPassword) {
        auth.signInWithEmailAndPassword(userEmail, userPassword).addOnCompleteListener((task) -> {
            if (task.isSuccessful()) {
                progressDialog.dismiss();
                String currentUser_id = auth.getCurrentUser().getUid();
                String token = FirebaseInstanceId.getInstance().getToken();
                userDatabase.child(currentUser_id)
                        .child("device_token")
                        .setValue(token).addOnSuccessListener(t -> {

                });

                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            } else {
                progressDialog.hide();
                Toast.makeText(LoginActivity.this,
                        "email or password not correct try Again !", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void showProgressDialog() {
        progressDialog.setTitle("Logging in");
        progressDialog.setMessage("please wait until login");
        progressDialog.show();
    }

    public boolean isValidUserInfo(String email, String password) {
        if (email.trim().isEmpty() || password.trim().isEmpty())
            return false;
        return true;
    }
}
