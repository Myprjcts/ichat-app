package project.university.com.ichatapp;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.*;

import de.hdodenhof.circleimageview.CircleImageView;

public class ChatActivity extends AppCompatActivity {

    private String chatUser;
    private Toolbar toolbar;
    private DatabaseReference rootRef;
    private TextView titleView;
    private TextView lastSeen;
    private CircleImageView profileImage;
    private FirebaseAuth auth;
    private String currentUser;
    private ImageButton sendBtn;
    private EditText textView;
    private RecyclerView messageList;
    private List<Message> messages = new ArrayList<>();
    private LinearLayoutManager linearLayoutManager;
    private MessageAdapter messageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        rootRef = FirebaseDatabase.getInstance().getReference();
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setDisplayShowCustomEnabled(true);
        chatUser = getIntent().getStringExtra("user_id");
        String username = getIntent().getStringExtra("username");
        //getSupportActionBar().setTitle(username);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View actionBarView = inflater.inflate(R.layout.chat_custom_bar, null);
        actionBar.setCustomView(actionBarView);
        sendBtn = findViewById(R.id.send_btn);
        textView = findViewById(R.id.chat_message_view);
        auth = FirebaseAuth.getInstance();
        currentUser = auth.getCurrentUser().getUid();
        titleView = findViewById(R.id.title_name);
        lastSeen = findViewById(R.id.last_seen);
        profileImage = findViewById(R.id.profile_image);
        messageList = findViewById(R.id.message_list);

        messageAdapter = new MessageAdapter(messages);
        linearLayoutManager = new LinearLayoutManager(this);
        messageList.setHasFixedSize(true);
        linearLayoutManager.setStackFromEnd(true);
        messageList.setLayoutManager(linearLayoutManager);
        messageList.setAdapter(messageAdapter);
        loadMessage();

        titleView.setText(username);
        rootRef.child("users").child(chatUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String online = dataSnapshot.child("online").getValue().toString();
                String image = dataSnapshot.child("image").getValue().toString();

                if (online.equals("true")) {
                    lastSeen.setText("online");
                } else {
                    TimeAgo timeAgo = new TimeAgo();
                    long sinceDate = Long.parseLong(online);
                    String lastSeenTime = timeAgo.getTimeAgo(sinceDate, getApplicationContext());

                    lastSeen.setText(lastSeenTime);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        rootRef.child("chat").child(currentUser).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (!dataSnapshot.hasChild(chatUser)) {
                    Map chatMap = new HashMap();
                    chatMap.put("seen", false);
                    chatMap.put("timestamp", ServerValue.TIMESTAMP);

                    Map convMap = new HashMap();
                    convMap.put("chat/" + currentUser + "/" + chatUser, chatMap);
                    convMap.put("chat/" + chatUser + "/" + currentUser, chatMap);

                    rootRef.updateChildren(convMap, new DatabaseReference.CompletionListener() {
                        @Override
                        public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                            if (databaseError != null) {
                                Log.d("chat error", databaseError.getMessage().toString());
                            }
                        }
                    });
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        sendBtn.setOnClickListener((view) -> {
            sendMessage();
            messageList.smoothScrollToPosition(messages.size());
            textView.setText("");
        });
    }

    private void loadMessage() {
        rootRef.child("messages").child(currentUser).child(chatUser).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Message message = dataSnapshot.getValue(Message.class);
                messages.add(message);
                messageAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sendMessage() {

        System.out.println("HEREEEEEEEEEEE");
        String message = textView.getText().toString();

        if (!TextUtils.isEmpty(message)) {
            String currentUserRef = "messages/" + currentUser + "/" + chatUser;
            String chatUserRef = "messages/" + chatUser + "/" + currentUser;

            DatabaseReference messagePushVal = rootRef.child("messages")
                    .child(currentUser)
                    .child(chatUser).push();

            String pushId = messagePushVal.getKey();

            Map messageMap = new HashMap();
            messageMap.put("message", message);
            messageMap.put("seen", false);
            messageMap.put("type", "Text");
            messageMap.put("timestamp", ServerValue.TIMESTAMP);
            messageMap.put("from", currentUser);

            Map messageUserMap = new HashMap();

            messageUserMap.put(currentUserRef + "/" + pushId, messageMap);
            messageUserMap.put(chatUserRef + "/" + pushId, messageMap);

            rootRef.updateChildren(messageUserMap, (databaseError, databaseRef) -> {
                if (databaseError != null) {
                    Log.d("chat error", databaseError.getMessage().toString());
                }
            });
        }
    }
}
