package project.university.com.ichatapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;

public class StartActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private Button reg;
    private Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        reg = findViewById(R.id.RegisterBtn);
        login = findViewById(R.id.login);
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("IChat App");
        login.setOnClickListener((view) -> moveToRegisterActivity(LoginActivity.class));
        reg.setOnClickListener((view) -> moveToRegisterActivity(RegisterActivity.class));
    }

    private void moveToRegisterActivity(Class nextActivity) {
        Intent intent = new Intent(StartActivity.this, nextActivity);
        startActivity(intent);
    }
}