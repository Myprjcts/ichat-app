package project.university.com.ichatapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    private static Map<String, RequestProcessor> reqProcessorMap;
    private ImageView userProfImage;
    private TextView userPofName, userProfStatus, userFriendCount;
    private Button requestButton;
    private Button declineReq;
    private DatabaseReference dbReference;
    private DatabaseReference dbReqRef;
    private ProgressDialog progressDialog;
    private String userCurrentState;
    private String currentUser;
    private DatabaseReference dbFriends;
    private DatabaseReference notificationDB;
    private DatabaseReference rootRef;

    {
        reqProcessorMap = new HashMap<>();
        reqProcessorMap.put("not friend", this::sendFriendRequest);
        reqProcessorMap.put("req_sent", this::CancelFriendRequest);
        reqProcessorMap.put("req_received", this::processFriendSinceDate);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        String user_id = getIntent().getStringExtra("user_id");
        dbReference = FirebaseDatabase.getInstance().getReference().child("users").child(user_id);
        rootRef = FirebaseDatabase.getInstance().getReference();
        notificationDB = FirebaseDatabase.getInstance().getReference().child("notifications");
        dbReference.keepSynced(true);
        dbReqRef = FirebaseDatabase.getInstance().getReference().child("Friend_req");
        dbFriends = FirebaseDatabase.getInstance().getReference().child("Friends");
        currentUser = FirebaseAuth.getInstance().getCurrentUser().getUid();
        userProfImage = findViewById(R.id.user_prof_image);
        userPofName = findViewById(R.id.user_prof_name);
        userProfStatus = findViewById(R.id.user_prof_status);
        requestButton = findViewById(R.id.request_button);
        declineReq = findViewById(R.id.DeclineReq);
        userFriendCount = findViewById(R.id.user_total_friends);
        userCurrentState = "not friend";

        declineReq.setVisibility(View.INVISIBLE);
        declineReq.setEnabled(false);

        addUserDataLoadingDialog();

        dbReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                updateUi(dataSnapshot);
                updateRequestButton(user_id);
                updateFriendStatusUI(user_id);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        requestButton.setOnClickListener((view) -> {
            requestButton.setEnabled(false);
            reqProcessorMap.get(userCurrentState).process(user_id);
        });
    }

    private void updateFriendStatusUI(String user_id) {
        dbFriends.child(currentUser).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.hasChild(user_id)) {
                    userCurrentState = "friend";
                    requestButton.setText("UnFriend This User");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    private void sendFriendRequest(String user_id) {

        DatabaseReference notificationRef = rootRef.child("notifications").child(user_id).push();
        String notificationId = notificationRef.getKey();
        Map<String, String> notificationData = new HashMap<>();
        notificationData.put("from", currentUser);
        notificationData.put("type", "request");

        Map reqMap = new HashMap<>();
        reqMap.put("Friend_req/" + currentUser + "/" + user_id +"/"+ "request_type", "sent");
        reqMap.put("Friend_req/" + user_id + "/" + currentUser +"/"+ "request_type", "received");
        reqMap.put("notifications/" + user_id + "/" + notificationId, notificationData);

        rootRef.updateChildren(reqMap, new DatabaseReference.CompletionListener() {
            @Override
            public void onComplete(DatabaseError databaseError, DatabaseReference databaseReference) {
                if (databaseError != null) {
                    Toast.makeText(ProfileActivity.this, "error while sending a request", Toast.LENGTH_SHORT).show();
                } else {
                    requestButton.setText("Cancel Friend Request");
                    requestButton.setEnabled(true);
                    userCurrentState = "req_sent";
                }
            }
        });
    }

    private void CancelFriendRequest(String user_id) {
        dbReqRef.child(currentUser)
                .child(user_id)
                .removeValue()
                .addOnSuccessListener((task) -> {
                    dbReqRef.child(user_id).child(currentUser)
                            .removeValue()
                            .addOnSuccessListener((t) -> {
                                requestButton.setEnabled(true);
                                userCurrentState = "not friend";
                                requestButton.setText("Send Friend Request");
                            });
                });
    }

    private void processFriendSinceDate(String user_id) {
        final String currentDate = DateFormat.getDateInstance().format(new Date());

        dbFriends.child(currentUser).child(user_id).child("date").setValue(currentDate)
                .addOnSuccessListener((task) -> {
                    dbFriends.child(user_id).child(currentUser).child("date").setValue(currentDate)
                            .addOnSuccessListener((innerTask) -> {
                                dbReqRef.child(currentUser)
                                        .child(user_id)
                                        .removeValue()
                                        .addOnSuccessListener((t) -> {
                                            dbReqRef.child(user_id).child(currentUser)
                                                    .removeValue()
                                                    .addOnSuccessListener((t2) -> {
                                                        requestButton.setEnabled(true);
                                                        userCurrentState = "friend";
                                                        requestButton.setText("UnFriend This User");
                                                    });
                                        });
                            });
                });
    }

    private void updateUi(DataSnapshot dataSnapshot) {
        String userName = dataSnapshot.child("name").getValue().toString();
        String userStatus = dataSnapshot.child("status").getValue().toString();
        String userImage = dataSnapshot.child("image").getValue().toString();
        userPofName.setText(userName);
        userProfStatus.setText(userStatus);
        Picasso.with(ProfileActivity.this).load(userImage).placeholder(R.drawable.finalpic)
                .into(userProfImage);
        progressDialog.dismiss();
    }

    private void updateRequestButton(String user_id) {
        dbReqRef.child(currentUser)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.hasChild(user_id)) {
                            String request_type = dataSnapshot.child(user_id).child("request_type").getValue().toString();
                            if (request_type.trim().equalsIgnoreCase("received")) {
                                userCurrentState = "req_received";
                                requestButton.setText("Accept Friend Request");
                                declineReq.setVisibility(View.VISIBLE);
                                declineReq.setEnabled(true);
                            } else if (request_type.trim().equalsIgnoreCase("sent")) {
                                userCurrentState = "req_sent";
                                requestButton.setText("Cancel Friend Request");
                                declineReq.setVisibility(View.INVISIBLE);
                                declineReq.setEnabled(false);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    private void addUserDataLoadingDialog() {
        progressDialog = new ProgressDialog(this);

        progressDialog.setTitle("Loading Profile User");
        progressDialog.setMessage("Please Wait until user data loaded");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private interface RequestProcessor {
        void process(String state);
    }
}
