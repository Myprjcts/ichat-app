package project.university.com.ichatapp;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;


public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    private List<Message> messageList;
    private FirebaseAuth auth;

    public MessageAdapter(List<Message> messageList) {
        this.messageList = messageList;
        this.auth = FirebaseAuth.getInstance();
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_single_layout, parent, false);

        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {
        Message message = messageList.get(position);
        String currentUserId = auth.getCurrentUser().getUid();

        if (currentUserId.equals(message.getFrom())) {
            holder.message.setBackgroundResource(R.drawable.message_text_back_to);
            holder.message.setTextColor(Color.BLACK);
        } else {
            holder.message.setBackgroundResource(R.drawable.message_text_back);
            holder.message.setTextColor(Color.WHITE);
        }

        holder.message.setText(message.getMessage());
        /*Picasso.with(MessageAdapter.th).load(userImage).placeholder(R.drawable.finalpic)
                .into(userProfImage);*/
        //holder.profileImage.setText(message.getTime());
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public class MessageViewHolder extends RecyclerView.ViewHolder {
        private TextView message;
        private CircleImageView profileImage;

        public MessageViewHolder(View view) {
            super(view);
            this.message = view.findViewById(R.id.message_text_layout);
            this.profileImage = view.findViewById(R.id.message_profile_layout);
        }
    }
}
