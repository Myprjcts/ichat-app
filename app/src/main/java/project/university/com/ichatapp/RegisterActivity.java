package project.university.com.ichatapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity {

    private TextInputLayout displayName;
    private TextInputLayout email;
    private TextInputLayout pasword;
    private Button btn;
    private FirebaseAuth auth;
    private Toolbar toolbar;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        auth = FirebaseAuth.getInstance();
        displayName = findViewById(R.id.dispName);
        email = findViewById(R.id.Email);
        pasword = findViewById(R.id.password);
        btn = findViewById(R.id.create);
        progressDialog = new ProgressDialog(this);
        addToolbar();

        btn.setOnClickListener(view -> {
            String user_name = displayName.getEditText().getText().toString().trim();
            String user_email = email.getEditText().getText().toString().trim();
            String user_password = pasword.getEditText().getText().toString().trim();

            if (!(user_email.isEmpty() || user_name.isEmpty() || user_password.isEmpty())) {
                showProgressBar();
                registerUser(user_name, user_email, user_password);
            }
        });
    }

    private void showProgressBar() {
        progressDialog.setTitle("Registering User");
        progressDialog.setMessage("Please Wait until create a new Account");
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.show();
    }

    private void addToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Create Account");
    }

    private void registerUser(String displayName, String user_email, String user_password) {
        auth.createUserWithEmailAndPassword(user_email, user_password)
                .addOnCompleteListener((task) -> {
                    if (task.isSuccessful()) {

                        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                        String userID = currentUser.getUid();
                        DatabaseReference db = FirebaseDatabase.getInstance().getReference().child("users").child(userID);
                        Map<String, String> fields = new HashMap<>();
                        fields.put("name", displayName);
                        fields.put("status", "Hi there , im using IChat App");
                        fields.put("image", "default");
                        fields.put("thumb_image", "default");
                        db.setValue(fields).addOnCompleteListener((t) -> {
                            if (t.isSuccessful()) {
                                progressDialog.dismiss();
                                Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                                finish();
                            }
                        });
                    } else {
                        progressDialog.hide();
                        Toast.makeText(RegisterActivity.this, "You have some error"
                                , Toast.LENGTH_LONG).show();
                    }
                });
    }
}
