package project.university.com.ichatapp;

import android.app.Service;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

import project.university.com.ichatapp.com.university.chat.fragments.SectionPagerAdapter;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private Toolbar toolbar;
    private ViewPager viewPager;
    private SectionPagerAdapter adapter;
    private TabLayout tableLayout;
    private DatabaseReference userDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        auth = FirebaseAuth.getInstance();

        toolbar = findViewById(R.id.main_page_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("IChat App");

        viewPager = findViewById(R.id.view_pager);

        if (auth.getCurrentUser() != null)
            userDb = FirebaseDatabase.getInstance().getReference().child("users").child(auth.getCurrentUser().getUid());

        adapter = new SectionPagerAdapter(getSupportFragmentManager());

        viewPager.setAdapter(adapter);

        tableLayout = findViewById(R.id.main_tabs);

        tableLayout.setupWithViewPager(viewPager);

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = auth.getCurrentUser();
        updateUI(currentUser);
    }

    private void updateUI(FirebaseUser user) {
        if (!isUserLogin(user)) {
            moveToStart();
        } else {
            userDb.child("online").onDisconnect().setValue(true);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (userDb != null)
            userDb.child("online").setValue(ServerValue.TIMESTAMP);
    }

    private boolean isUserLogin(FirebaseUser user) {
        if (user == null)
            return false;

        return true;
    }

    private void moveToStart() {
        Intent intent = new Intent(MainActivity.this, StartActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        if (item.getItemId() == R.id.sign_out) {
            FirebaseAuth.getInstance().signOut();
            userDb.child("online").setValue(ServerValue.TIMESTAMP);
            moveToStart();
        }

        if (item.getItemId() == R.id.account_settings) {
            Intent intent = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(intent);
        }

        if (item.getItemId() == R.id.all_users_menu) {
            Intent intent = new Intent(MainActivity.this, UsersActivity.class);
            startActivity(intent);
        }
        return true;
    }
}