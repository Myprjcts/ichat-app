package project.university.com.ichatapp.com.university.chat.fragments;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
import project.university.com.ichatapp.ChatActivity;
import project.university.com.ichatapp.Friends;
import project.university.com.ichatapp.ProfileActivity;
import project.university.com.ichatapp.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class FriendsFragment extends Fragment {

    private RecyclerView friendsList;
    private DatabaseReference friendsDb;
    private FirebaseAuth auth;
    private String current_user_id;
    private View mainView;
    private DatabaseReference userDb;

    public FriendsFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mainView = inflater.inflate(R.layout.fragment_friends, container, false);
        userDb = FirebaseDatabase.getInstance().getReference().child("users");
        friendsList = (RecyclerView) mainView.findViewById(R.id.friends_list);
        auth = FirebaseAuth.getInstance();
        current_user_id = auth.getCurrentUser().getUid();
        friendsDb = FirebaseDatabase.getInstance().getReference().child("Friends").child(current_user_id);
        friendsDb.keepSynced(true);
        userDb.keepSynced(true);
        friendsList.setHasFixedSize(true);
        friendsList.setLayoutManager(new LinearLayoutManager(getContext()));
        return mainView;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseRecyclerAdapter<Friends, FriendsViewHolder> recyclerAdapter = new FirebaseRecyclerAdapter<Friends, FriendsViewHolder>(
                Friends.class,
                R.layout.users_single_layout,
                FriendsViewHolder.class,
                friendsDb
        ) {
            @Override
            protected void populateViewHolder(FriendsViewHolder viewHolder, Friends model, int position) {
                viewHolder.setDate(model.getDate());
                String listUserId = getRef(position).getKey();
                userDb.child(listUserId).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String name = dataSnapshot.child("name").getValue().toString();
                        String thumb = dataSnapshot.child("thumb_image").getValue().toString();
                        String online_status = dataSnapshot.child("online").getValue().toString();
                        viewHolder.setName(name);
                        viewHolder.setImage(thumb, getContext());
                        viewHolder.setOnlineStatusIcon(online_status);
                        viewHolder.view.setOnClickListener(view -> {
                            String[] options = {"Open Profile", "Send message"};
                            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                            builder.setTitle("Select Option");
                            builder.setItems(options, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int position) {
                                    // determine which option selected

                                    if (position == 0) {
                                        Intent intent = new Intent(getContext(), ProfileActivity.class);
                                        intent.putExtra("user_id", listUserId);
                                        startActivity(intent);
                                    } else if (position == 1) {
                                        Intent intent = new Intent(getContext(), ChatActivity.class);
                                        intent.putExtra("user_id", listUserId);
                                        intent.putExtra("username", name);
                                        startActivity(intent);
                                    }
                                }
                            });
                            builder.show();
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        };

        friendsList.setAdapter(recyclerAdapter);
    }


    public static class FriendsViewHolder extends RecyclerView.ViewHolder {
        View view;

        public FriendsViewHolder(View itemView) {
            super(itemView);
            view = itemView;
        }

        public void setDate(String date) {
            TextView statusView = view.findViewById(R.id.user_single_status);
            statusView.setText(date);
        }

        public void setName(String name) {
            TextView nameView = view.findViewById(R.id.user_single_name);
            nameView.setText(name);
        }

        public void setImage(String image, Context context) {
            CircleImageView userImageView = (CircleImageView) view.findViewById(R.id.user_single_image);
            Picasso.with(context).load(image).placeholder(R.drawable.avatar).into(userImageView);
        }

        public void setOnlineStatusIcon(String online_status) {
            ImageView onlineIcon = view.findViewById(R.id.available_icon);
            if (online_status.equals("true")) {
                onlineIcon.setVisibility(View.VISIBLE);
            } else
                onlineIcon.setVisibility(View.INVISIBLE);
        }
    }
}
