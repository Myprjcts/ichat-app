package project.university.com.ichatapp;

import android.app.ProgressDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class StatusActivity extends AppCompatActivity {

    Toolbar toolbar;
    Button statusBtn;
    TextInputLayout statusInput;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);
        toolbar = findViewById(R.id.toolbar);
        statusBtn = findViewById(R.id.status_btn);
        statusInput = findViewById(R.id.status_input);
        progressDialog = new ProgressDialog(this);
        setupToolbar();
        String status_value = getIntent().getStringExtra("status_value");
        statusInput.getEditText().setText(status_value);
        statusBtn.setOnClickListener((view) -> changeStatus());
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Account Status");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void changeStatus() {
        String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
        DatabaseReference dbUser = FirebaseDatabase.getInstance().getReference()
                .child("users")
                .child(userId);
        progressDialog.setTitle("Saving Changes");
        progressDialog.setMessage("please wait until saving status");
        progressDialog.show();
        String userStatus = statusInput.getEditText().getText().toString();
        dbUser.child("status").setValue(userStatus).addOnCompleteListener((task) -> {
            if (task.isSuccessful()) {
                progressDialog.dismiss();
            } else
                Toast.makeText(getApplicationContext(),
                        "Some Error occurd when saving new status", Toast.LENGTH_LONG).show();
        });
    }
}
