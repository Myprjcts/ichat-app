package project.university.com.ichatapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

public class UsersActivity extends AppCompatActivity {

    Toolbar toolbar;
    RecyclerView mUsersList;

    DatabaseReference databaseReference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users);
        databaseReference = FirebaseDatabase.getInstance().getReference().child("users");
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("All Users");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mUsersList = findViewById(R.id.users_list);
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(new LinearLayoutManager(this));
    }


    @Override
    protected void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<User, UsersViewHolder> adapter = new FirebaseRecyclerAdapter<User, UsersViewHolder>(
                User.class,
                R.layout.users_single_layout,
                UsersViewHolder.class,
                databaseReference
        ) {
            @Override
            protected void populateViewHolder(UsersViewHolder viewHolder, User user, int position) {
                viewHolder.setDisplayName(user.getName());
                viewHolder.setUserStatus(user.getStatus());
                viewHolder.setImage(user.getThumb_image(), getApplicationContext());
                viewHolder.view.setOnClickListener((view) -> {
                    String user_id = getRef(position).getKey();
                    Intent intent = new Intent(UsersActivity.this, ProfileActivity.class);
                    intent.putExtra("user_id", user_id);
                    startActivity(intent);
                });

                viewHolder.view.findViewById(R.id.available_icon).setVisibility(View.INVISIBLE);
            }
        };

        mUsersList.setAdapter(adapter);
    }

    public static class UsersViewHolder extends RecyclerView.ViewHolder {
        View view;

        public UsersViewHolder(View itemView) {
            super(itemView);
            view = itemView;
        }

        public void setDisplayName(String name) {
            TextView friendName = view.findViewById(R.id.user_single_name);
            friendName.setText(name);
        }

        public void setUserStatus(String status) {
            TextView friendName = view.findViewById(R.id.user_single_status);
            friendName.setText(status);
        }

        public void setImage(String image, Context context) {
            CircleImageView userImageView = (CircleImageView) view.findViewById(R.id.user_single_image);
            Picasso.with(context).load(image).placeholder(R.drawable.avatar).into(userImageView);
        }
    }

}
