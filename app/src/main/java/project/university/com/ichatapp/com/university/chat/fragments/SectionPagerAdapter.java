package project.university.com.ichatapp.com.university.chat.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.HashMap;
import java.util.Map;

import project.university.com.ichatapp.com.university.chat.fragments.ChatFragment;
import project.university.com.ichatapp.com.university.chat.fragments.FriendsFragment;
import project.university.com.ichatapp.com.university.chat.fragments.RequestFragment;

/**
 * Created by user on 03/07/18.
 */

public class SectionPagerAdapter extends FragmentPagerAdapter {
    private static Map<Integer, Object> fragments = new HashMap<>();
    private static Map<Integer, String> hintMap = new HashMap<>();

    public SectionPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
        preparedFragments();
        preparedHint();
    }

    @Override
    public Fragment getItem(int position) {
        return (Fragment) fragments.get(position);
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return hintMap.get(position);
    }

    private void preparedFragments() {
        fragments.put(0, new RequestFragment());
        fragments.put(1, new ChatFragment());
        fragments.put(2, new FriendsFragment());
    }

    private void preparedHint() {
        hintMap.put(0, "REQUESTS");
        hintMap.put(1, "CHATS");
        hintMap.put(2, "FRIENDS");
    }
}
