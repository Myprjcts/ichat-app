package project.university.com.ichatapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
import id.zelory.compressor.Compressor;

public class SettingsActivity extends AppCompatActivity {

    private DatabaseReference databaseReference;
    private FirebaseUser currentUser;
    private CircleImageView imageView;
    private TextView displayName;
    private TextView user_status;
    private Button statusBtn;
    private Button imageBtn;
    private ProgressDialog dialog;
    private static final int REQUEST_CODE = 1;
    private StorageReference firebaseStorage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        imageView = findViewById(R.id.profile_pic);
        displayName = findViewById(R.id.display_name);
        user_status = findViewById(R.id.user_status);
        statusBtn = findViewById(R.id.setting_status);
        imageBtn = findViewById(R.id.image_btn);

        firebaseStorage = FirebaseStorage.getInstance().getReference();
        currentUser = FirebaseAuth.getInstance().getCurrentUser();
        String user_id = currentUser.getUid();
        databaseReference = FirebaseDatabase.getInstance().getReference().child("users").child(user_id);

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                updateUserData(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        statusBtn.setOnClickListener((view) -> {
            Intent statusIntent = new Intent(SettingsActivity.this, StatusActivity.class);
            String statusValue = user_status.getText().toString();
            statusIntent.putExtra("status_value", statusValue);
            startActivity(statusIntent);
        });

        imageBtn.setOnClickListener((view) -> {
            Intent galleryIntent = new Intent();
            galleryIntent.setType("image/*");
            galleryIntent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(galleryIntent, "Choose Image"), REQUEST_CODE);

            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this);
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            CropImage.activity(imageUri)
                    .setAspectRatio(1, 1)
                    .start(this);
        }

        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);

            if (resultCode == RESULT_OK) {
                dialog = new ProgressDialog(this);
                dialog.setTitle("Uploading image ...");
                dialog.setMessage("please wait until upload and process the image");
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();

                String user_id = currentUser.getUid();
                Uri resultUri = result.getUri();

                File file = new File(resultUri.getPath());

                Bitmap bitmap = new Compressor(this)
                        .setMaxWidth(200)
                        .setMaxHeight(200)
                        .setQuality(75)
                        .compressToBitmap(file);

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                byte[] imageBytes = baos.toByteArray();

                StorageReference filePath = firebaseStorage
                        .child("profile_images").child(user_id + ".jpg");

                StorageReference thumbPath = firebaseStorage.child("profile_images").child("thumbs").child(user_id + ".jpg");


                filePath.putFile(resultUri).addOnCompleteListener((task) -> {
                    if (task.isSuccessful()) {
                        String downloadUrl = task.getResult().getDownloadUrl().toString();
                        UploadTask uploadTask = thumbPath.putBytes(imageBytes);
                        uploadTask.addOnCompleteListener((thumbTask) -> {
                            String thumbDownloadUrl = thumbTask.getResult().getDownloadUrl().toString();
                            if (thumbTask.isSuccessful()) {
                                Map updateMap = new HashMap<>();
                                updateMap.put("image", downloadUrl);
                                updateMap.put("thumb_image", thumbDownloadUrl);

                                databaseReference.updateChildren(updateMap)
                                        .addOnCompleteListener((t) -> {
                                            if (t.isSuccessful()) {
                                                dialog.dismiss();
                                                Toast.makeText(this, "Successful", Toast.LENGTH_LONG).show();
                                            } else
                                                Toast.makeText(this, t.getException().getMessage(), Toast.LENGTH_LONG).show();
                                        });
                            }
                        });
                    } else
                        Toast.makeText(this, "Error while upload image", Toast.LENGTH_LONG).show();
                });
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    /*public String generateRandomNumber() {
        Random random = new Random();

        StringBuilder sb = new StringBuilder();

        int numberLength = random.nextInt(10);

        for (int i = 0; i <= numberLength; i++) {
            char tempChar = (char) (random.nextInt(96) + 32);
            sb.append(tempChar);
        }

        return sb.toString();
    }*/

    private void updateUserData(DataSnapshot dataSnapshot) {
        String name = dataSnapshot.child("name").getValue().toString();
        String status = dataSnapshot.child("status").getValue().toString();
        String image = dataSnapshot.child("image").getValue().toString();
        String thumbImage = dataSnapshot.child("thumb_image").getValue().toString();
        displayName.setText(name);
        user_status.setText(status);

        if (!image.equalsIgnoreCase("image_link"))
            Picasso.with(SettingsActivity.this).load(thumbImage)
                    .placeholder(R.drawable.avatar)
                    .networkPolicy(NetworkPolicy.OFFLINE)
                    .into(imageView, new Callback() {
                        @Override
                        public void onSuccess() {

                        }

                        @Override
                        public void onError() {
                            Picasso.with(SettingsActivity.this).load(thumbImage)
                                    .placeholder(R.drawable.avatar)
                                    .into(imageView);
                        }
                    });

    }
}
